# CIn Requests System

This is a requests system developed as a challenge. Node v18.17.1 was used.

## Environment Setup

Before you begin, make sure you have Docker installed on your system. If you don't have it installed yet, you can download it [here](https://www.docker.com/get-started/).

1. Clone this repository to your development environment:
```bash
git clone https://gitlab.com/raulpcoelho/cin-reqsystem
cd cin-reqsystem

```

2. Create a file named .env in the server folder to store the database environment variables. An example .env file:

```bash
POSTGRES_USER=postgres
POSTGRES_PASSWORD=psswd
POSTGRES_DB=cinrequests-dk

```

3. Still in the server folder, run:

```
npm install
```

Start the server with docker:
```
docker-compose up --build
```

Open another terminal in the server folder and execute the following command to apply migrations and seeds:
```
docker exec -it node-api-1 sh -c "cd /app && npm run migrate && npm run seed"
```

4. In the client folder, run:

```
npm install
npm start
```
This will start the client and open the application in your default web browser.

## Usage
- On the "Requests" link, students can track their requests and submit new ones.

- On the "SecGrad" link, SecGrad administrators can view requests made to the department and close them.

- On the "GerSist" link, GerSist administrators can view requests made to the department and close them.

## API Swagger docs
Visit http://localhost:1337/api-docs to view the API documentation!

## MER

See MER on server/MER.png!

## To Be Implemented

The current system allows students to access requests from various students by simply altering the URL route. To enhance security and students' privacy, implementing a login system is essential. This will ensure that each student can only view their own requests, adding an authentication layer.

In addition, the system should be enhanced with the following features:
- Student Registration: Implement a registration system so that students who do not have a login can create their accounts. This will ensure that all students have access to their own requests.
- Departmental Privacy: Currently, everybody can view all requests sent to the departments. Implementing a system that allows managers to only view requests submitted to their department will enhance privacy and improve request management efficiency.
- Data Encryption: To ensure the security of students' information and request data, implementing data encryption is crucial. 
- Improved Request Visualization: Enhance the user interface to make it easier to view and manage requests. This may include creating filters, sorting, and design improvements to make the system more user-friendly and efficient.
- Tests
