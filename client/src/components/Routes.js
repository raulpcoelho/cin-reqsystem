import React, { Component } from "react";
import { Route, Routes } from "react-router-dom";

import StudentRequests from "./StudentRequests";
import DepartmentRequests from "./DepartmentRequests";
import Home from "./Home";
import PageTest from "./PageTest";


class RouteOptions extends Component {
  render() {
    return (
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/requests/student/:studentId" element={<StudentRequests />} />
          <Route path="/requests/department/:departmentId" element={<DepartmentRequests />} />
          <Route path="/pagetest" element={<PageTest />} />

        </Routes>
    );
  }
}

export default RouteOptions;
