import React, { Component } from "react";
import { Link } from "react-router-dom";
import { withRouter } from "../../utils/misc";
import "./Home.css";

class Home extends Component {
  render() {
    return (
      <div className="container mt-3">
        <h2>Welcome to our CIn Request System!</h2>
        <h5 className="options-title">Navigate using the header or the options below:</h5>

        <p>
          If you are a student, go to {" "}
          <Link to="/requests/student/1" className="App-link">
            Requests.
          </Link>
        </p>

        <p>
          If you are from SecGrad, go to {" "}
          <Link to="/requests/department/1" className="App-link">
            SecGrad.
          </Link>
        </p>

        <p>
          If you are from GerSist, go to {" "}
          <Link to="/requests/department/2" className="App-link">
            GerSist.
          </Link>
        </p>

        <p>
          Login test page {" "}
          <Link to="/pagetest" className="App-link">
            here.
          </Link>

        </p>
      </div>
    );
  }
}

export default withRouter(Home);
