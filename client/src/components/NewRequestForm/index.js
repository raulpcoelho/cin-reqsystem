import React, { Component } from "react";
import "./NewRequestForm.css";

class NewRequestForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      department: "SecGrad",
      request_type: "",
      description: "",
    };
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleSubmit = async (e) => {
    e.preventDefault();

    const requestData = {
      request_type: this.state.request_type,
      request_date: new Date().toLocaleDateString(), // Current date
      request_time: new Date().toLocaleTimeString(), // Current time
      department_id: this.state.department === "SecGrad" ? 1 : 2,
      student_id: this.props.studentId,
      status: "Pending",
      description: this.state.description,
    };

    try {
      const response = await fetch("http://localhost:1337/requests", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(requestData),
      });

      if (!response.ok) {
        throw new Error("Error in network");
      }

      this.setState({
        department: "SecGrad",
        request_type: "",
        description: "",
      });
      this.props.fetchRequests();
      console.log("Request submitted successfully!");
    } catch (error) {
      console.error("Error submitting request:", error);
    }
  };

  render() {
    const { department, request_type, description } = this.state;

    return (
      <form onSubmit={this.handleSubmit} className="my-form">
        <div className="form-line row">
          <div className="col-md-2">
            <label htmlFor="requestDepartment" className="form-label">
              Department
            </label>
            <select
              id="requestDepartment"
              className="form-select"
              aria-label="Select department"
              name="department"
              value={department}
              onChange={this.handleChange}
            >
              <option value="SecGrad">SecGrad</option>
              <option value="GerSist">GerSist</option>
            </select>
          </div>
          <div className="col">
            <label htmlFor="requestType" className="form-label">
              Type
            </label>
            <input
              type="text"
              id="requestType"
              className="form-control"
              name="request_type"
              value={request_type}
              onChange={this.handleChange}
              required
            />
          </div>
        </div>

        <div className="form-line">
          <label htmlFor="requestDescription" className="form-label">
            Description
          </label>
          <textarea
            id="requestDescription"
            className="form-control"
            name="description"
            value={description}
            onChange={this.handleChange}
            rows="3"
            required
          />
        </div>
        <button type="submit" className="btn">
          Submit
        </button>
      </form>
    );
  }
}

export default NewRequestForm;
