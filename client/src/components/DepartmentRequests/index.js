import React, { Component } from "react";
import { formatDate, withRouter } from "../../utils/misc";
import "./DepartmentRequests.css";

class DepartmentRequests extends Component {
  constructor(props) {
    super(props);
    this.state = {
      departmentId: props.router.params.departmentId,
      requests: [],
      isLoading: true,
      error: null,
    };
  }

  fetchRequests = async () => {
    try {
      const response = await fetch(
        `http://localhost:1337/requests/department/${this.state.departmentId}`
      );
      if (!response.ok) {
        throw new Error("Error in network");
      }
      const data = await response.json();
      this.setState({ requests: data, isLoading: false });
    } catch (error) {
      this.setState({ error, isLoading: false });
      console.error("Error fetching department requests:", error);
    }
  };

  async componentDidMount() {
    this.fetchRequests();
  }

  handleRequestClose = async (requestId) => {
    try {
      const response = await fetch(
        `http://localhost:1337/requests/close/${requestId}`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({ status: "Closed" }),
        }
      );

      if (!response.ok) {
        throw new Error("Error closing request");
      }

      this.fetchRequests();
    } catch (error) {
      console.error("Error closing request:", error);
    }
  };

  render() {
    const { requests, isLoading, error } = this.state;

    return (
      <div className="department-requests">
        <div className="container mt-3">
          {this.state.departmentId === "1" ? (
            <h2>Requests to SecGrad</h2>
          ) : (
            <h2>Requests to GerSist</h2>
          )}
          {isLoading ? (
            <p>Loading...</p>
          ) : error ? (
            <p>Error: {error.message}</p>
          ) : (
            <table>
              <thead>
                <tr>
                  <th>Date</th>
                  <th>Time</th>
                  <th>Type</th>
                  <th>Student</th>
                  <th>Description</th>
                  <th>Status</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                {requests.map((request) => (
                  <tr key={request.id}>
                    <td>{formatDate(request.request_date)}</td>
                    <td>{request.request_time}</td>
                    <td>{request.request_type}</td>
                    <td>{request.student_name}</td>
                    <td>{request.description}</td>
                    <td>{request.status}</td>
                    <td>
                      {request.status === "Pending" && (
                        <button
                          onClick={() => this.handleRequestClose(request.id)}
                          className="btn btn-danger"
                        >
                          Close Request
                        </button>
                      )}
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          )}
        </div>
      </div>
    );
  }
}

export default withRouter(DepartmentRequests);
