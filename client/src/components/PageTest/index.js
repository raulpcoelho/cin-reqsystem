import React, { Component } from "react";
import { withRouter } from "../../utils/misc";
import "./PageTest.css";

class PageTest extends Component {
  render() {
    return (
      <>
        <div className="my-page container mt-3">
          <p className="title">Sign in as a student or administrator</p>
        </div>

        <div className="my-page">
          <div className="my-box">
            <form>
              <div>
                <label htmlFor="emailUsername" className="label-cont">
                  Email address
                </label>
                <input
                  className="my-input"
                  type="email"
                  id="emailUsername"
                  aria-describedby="emailHelp"
                ></input>
              </div>
              <div>
                <label htmlFor="passwordForm" className="label-cont">
                  Password
                </label>
                <input
                  className="my-input"
                  type="password"
                  id="passwordForm"
                ></input>
              </div>
              <button type="submit" className="btn-sign-in">
                Sign in
              </button>
            </form>
          </div>
        </div>
      </>
    );
  }
}

export default withRouter(PageTest);
