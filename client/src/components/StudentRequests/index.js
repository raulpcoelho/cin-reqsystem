import React, { Component } from "react";
import { formatDate, withRouter } from "../../utils/misc";
import NewRequestForm from "../NewRequestForm";
import "./StudentRequests.css";

class StudentRequests extends Component {
  constructor(props) {
    super(props);
    this.state = {
      studentId: props.router.params.studentId,
      requests: [],
      isLoading: true,
      error: null,
    };
  }

  fetchRequests = async () => {
    try {
      const response = await fetch(
        `http://localhost:1337/requests/student/${this.state.studentId}`
      );
      if (!response.ok) {
        throw new Error("Error in network");
      }
      const data = await response.json();
      this.setState({ requests: data, isLoading: false });
    } catch (error) {
      this.setState({ error, isLoading: false });
      console.error("Error fetching student requests:", error);
    }
  };

  async componentDidMount() {
    this.fetchRequests();
  }

  render() {
    const { requests, isLoading, error } = this.state;

    return (
      <>
        <div className="container mt-3">
          <h2>Make New Request</h2>
          <NewRequestForm studentId={this.state.studentId} fetchRequests={this.fetchRequests}/>
        </div>
        <div className="container mt-3">
          <h2>Your Requests</h2>
          {isLoading ? (
            <p>Loading...</p>
          ) : error ? (
            <p>Error: {error.message}</p>
          ) : (
            <table>
              <thead>
                <tr>
                  <th>Date</th>
                  <th>Time</th>
                  <th>Department</th>
                  <th>Type</th>
                  <th>Description</th>
                  <th>Status</th>                
                </tr>
              </thead>
              <tbody>
                {requests.map((request) => (
                  <tr key={request.id}>
                    <td>{formatDate(request.request_date)}</td>
                    <td>{request.request_time}</td>
                    <td>{request.department_name}</td>
                    <td>{request.request_type}</td>
                    <td>{request.description}</td>
                    <td>{request.status}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          )}
        </div>
      </>
    );
  }
}

export default withRouter(StudentRequests);
