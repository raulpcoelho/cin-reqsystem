import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import Header from './components/Header';
import RouteOptions from "./components/Routes";
import 'bootstrap/dist/js/bootstrap.bundle';
import './App.css';


class App extends React.Component {
  render() {
    return (
      <div className="App">
        <Header />
        <Router>
          <RouteOptions />
        </Router>
      </div>
    );
  }
}

export default App;
