require("dotenv").config();
const express = require("express");
const cors = require('cors');
const swaggerUi = require("swagger-ui-express");
const swaggerSpec = require("./swaggerConfig");

const routes = require("./routes");

const app = express();
const port = process.env.PORT || 1337;

app.use(express.json());
app.use(cors());

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerSpec));

app.use(routes);

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
