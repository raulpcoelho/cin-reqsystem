const db = require("../db");


exports.getStudents = async (req, res) => {
  try {
    const { rows } = await db.query("SELECT * FROM students");
    console.log(rows);
    res.status(200).json(rows);
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Error getting students." });
  }
}

exports.createStudent = async (req, res) => {
  const { name, semester, cpf } = req.body;
  try {
    const result = await db.query(
      "INSERT INTO students (name, semester, cpf) VALUES ($1, $2, $3) RETURNING *",
      [name, semester, cpf]
    );
    const newStudent = result.rows[0];
    res.status(201).json(newStudent);
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Error creating a new student." });
  }
}
