const db = require("../db");


exports.getDepartments = async (req, res) => {
  try {
    const { rows } = await db.query("SELECT * FROM departments");
    console.log(rows);
    res.status(200).json(rows);
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Error getting departments." });
  }
}

exports.createDepartment = async (req, res) => {
  const { name } = req.body;
  try {
    const result = await db.query(
      "INSERT INTO departments (name) VALUES ($1) RETURNING *",
      [name]
    );
    const newDepartment = result.rows[0];
    res.status(201).json(newDepartment);
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Error creating a new department." });
  }
}