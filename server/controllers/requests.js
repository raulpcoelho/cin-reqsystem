const db = require("../db");

exports.getRequest = async (req, res) => {
  try {
    const { rows } = await db.query("SELECT * FROM requests");
    res.status(200).json(rows);
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Error getting requests." });
  }
};

exports.getRequestsByStudent = async (req, res) => {
  const { studentId } = req.params;
  try {
    const { rows } = await db.query(
      "SELECT R.id, R.request_type, R.request_date, R.request_time, R.department_id, R.status, R.description,\
       R.department_id, D.name as department_name, R.student_id, S.name as student_name\
       FROM requests R INNER JOIN departments D ON R.department_id = D.id\
       INNER JOIN students S ON R.student_id = S.id\
       WHERE S.id = $1",
      [studentId]
    );
    res.status(200).json(rows);
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Error getting requests by student." });
  }
};

exports.getRequestsToDepartment = async (req, res) => {
  const { departmentId } = req.params;
  try {
    const { rows } = await db.query(
      "SELECT R.id, R.request_type, R.request_date, R.request_time, R.department_id, R.status, R.description,\
       R.department_id, D.name as department_name, R.student_id, S.name as student_name\
       FROM requests R INNER JOIN departments D ON R.department_id = D.id\
       INNER JOIN students S ON R.student_id = S.id\
       WHERE D.id = $1",
      [departmentId]
    );
    res.status(200).json(rows);
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Error getting requests by department." });
  }
};

exports.createRequest = async (req, res) => {
  const {
    request_type,
    request_date,
    request_time,
    student_id,
    department_id,
    status,
    description,
  } = req.body;

  try {
    const result = await db.query(
      "INSERT INTO requests (request_type, request_date, request_time, student_id, department_id, status, description)\
       VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING *",
      [
        request_type,
        request_date,
        request_time,
        student_id,
        department_id,
        status,
        description,
      ]
    );

    const createdRequest = result.rows[0];
    res.status(201).json(createdRequest);
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Error creating a new request." });
  }
};

exports.editRequest = async (req, res) => {
  try {
    const requestId = req.params.id;
    const { status } = req.body;

    const updateQuery = "UPDATE requests SET status = $1 WHERE id = $2";
    await db.query(updateQuery, [status, requestId]);

    return res
      .status(200)
      .json({ message: "Request status updated successfully" });
  } catch (error) {
    console.error("Error updating request status:", error);
    return res.status(500).json({ message: "Internal server error" });
  }
}
