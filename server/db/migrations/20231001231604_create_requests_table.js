/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
  return knex.schema.createTable('requests', function (table) {
    table.increments('id').primary();
    table.string('request_type').notNullable();
    table.date('request_date').notNullable();
    table.time('request_time').notNullable();
    table.integer('department_id').notNullable();
    table.integer('student_id').notNullable();
    table.string('status').notNullable();
    table.text('description').notNullable();
    table.timestamps(true, true);
  });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function(knex) {
  return knex.schema.dropTable('requests');
};
