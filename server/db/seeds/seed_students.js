/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */
exports.seed = async function(knex) {
  // Deletes ALL existing entries
  await knex('students').del()
  await knex('students').insert([
    {name: "John Doe", cpf: "111-111-111-11", semester: 6},
    {name: "Jane", cpf: "222-222-222-22", semester: 3}
  ]);
};
