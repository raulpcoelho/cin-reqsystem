/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */
exports.seed = async function(knex) {
  // Deletes ALL existing entries
  await knex('requests').del()
  await knex('requests').insert([
    {
      request_type: 'Type 1',
      request_date: '2023-09-30',
      request_time: '08:00:00',
      department_id: 1,
      student_id: 1,
      status: 'Pending',
      description: 'Description 1',
    },
    {
      request_type: 'Type 1',
      request_date: '2023-09-29',
      request_time: '07:00:00',
      department_id: 1,
      student_id: 1,
      status: 'Pending',
      description: 'Description 2',
    },
    {
      request_type: 'Type 2',
      request_date: '2023-09-27',
      request_time: '15:00:00',
      department_id: 1,
      student_id: 1,
      status: 'Pending',
      description: 'Description 3',
    },
    {
      request_type: 'Type 2',
      request_date: '2023-09-30',
      request_time: '16:34:00',
      department_id: 1,
      student_id: 2,
      status: 'Pending',
      description: 'Description 4',
    },
    {
      request_type: 'Type 3',
      request_date: '2023-09-29',
      request_time: '08:59:00',
      department_id: 2,
      student_id: 2,
      status: 'Pending',
      description: 'Description 5',
    },
    {
      request_type: 'Type 1',
      request_date: '2023-09-25',
      request_time: '09:00:00',
      department_id: 2,
      student_id: 1,
      status: 'Closed',
      description: 'Description 6',
    },
    {
      request_type: 'Type 4',
      request_date: '2023-09-30',
      request_time: '16:45:00',
      department_id: 1,
      student_id: 2,
      status: 'Closed',
      description: 'Description 7',
    },
    {
      request_type: 'Type 3',
      request_date: '2023-09-30',
      request_time: '16:10:00',
      department_id: 1,
      student_id: 1,
      status: 'Pending',
      description: 'Description 8',
    },
    {
      request_type: 'Type 4',
      request_date: '2023-09-28',
      request_time: '16:32:00',
      department_id: 2,
      student_id: 2,
      status: 'Closed',
      description: 'Description 9',
    },
    {
      request_type: 'Type 1',
      request_date: '2023-09-28',
      request_time: '12:45:00',
      department_id: 1,
      student_id: 1,
      status: 'Closed',
      description: 'Description 10',
    },
  ]);
};
