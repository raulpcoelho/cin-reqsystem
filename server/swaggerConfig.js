const swaggerJsdoc = require("swagger-jsdoc");

const options = {
  definition: {
    openapi: "3.0.0", // Specify the version of OpenAPI (Swagger)
    info: {
      title: "CIn Requests API",
      version: "1.0.0"
    },
  },
  // List your API files here
  apis: ["./routes.js"], // Path to your API route files
};

const swaggerSpec = swaggerJsdoc(options);

module.exports = swaggerSpec;
