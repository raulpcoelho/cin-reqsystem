const express = require("express");
const router = express.Router();

const { getStudents, createStudent } = require("./controllers/students");
const { getDepartments, createDepartment } = require("./controllers/departments");
const { getRequest, getRequestsByStudent, getRequestsToDepartment, createRequest, editRequest } = require("./controllers/requests");

/**
 * @swagger
 * /students:
 *   get:
 *     summary: Get a list of all students
 *     responses:
 *       200:
 *         description: List of students returned.
 *       500:
 *         description: Error getting students.
 */
router.get("/students", getStudents);

/**
 * @swagger
 * /departments:
 *   get:
 *     summary: Get a list of all departments
 *     responses:
 *       200:
 *         description: List of departments returned.
 *       500:
 *         description: Error getting departments.
 */
router.get("/departments", getDepartments);

/**
 * @swagger
 * /requests:
 *   get:
 *     summary: Get a list of all requests
 *     responses:
 *       200:
 *         description: List of requests returned.
 *       500:
 *         description: Error getting requests.
 */
router.get("/requests", getRequest);

/**
 * @swagger
 * /requests/student/{studentId}:
 *   get:
 *     summary: Get a list of all requests made by some student
 *     parameters:
 *      - in: path
 *        name: studentId
 *        required: true
 *        schema:
 *          type: integer
 *     responses:
 *       200:
 *         description: List of requests made by student returned.
 *       500:
 *         description: Error getting requests.
 */
router.get("/requests/student/:studentId", getRequestsByStudent);

/**
 * @swagger
 * /requests/department/{departmentId}:
 *   get:
 *     summary: Get a list of all requests made to some department
 *     parameters:
 *      - in: path
 *        name: departmentId
 *        required: true
 *        schema:
 *          type: integer
 *     responses:
 *       200:
 *         description: List of requests made to some department returned.
 *       500:
 *         description: Error getting requests.
 */
router.get("/requests/department/:departmentId", getRequestsToDepartment);

/**
 * @swagger
 * /students:
 *   post:
 *     summary: Create a student
 *     requestBody:
 *       description: User object
 *       required: true
 *       content:
 *         application/json:
 *           example:
 *             name: John Doe
 *             semester: 9
 *             cpf: 999-999-999-99 
 *     responses:
 *       201:
 *         description: Student created
 *       500:
 *         description: Error creating student
 */
router.post("/students", createStudent);

/**
 * @swagger
 * /departments:
 *   post:
 *     summary: Create a department
 *     requestBody:
 *       description: User object
 *       required: true
 *       content:
 *         application/json:
 *           example:
 *             name : "MyDep"
 *     responses:
 *       201:
 *         description: Department created
 *       500:
 *         description: Error creating department
 */
router.post("/departments", createDepartment);

/**
 * @swagger
 * /requests:
 *   post:
 *     summary: Create a request
 *     requestBody:
 *       description: User object
 *       required: true
 *       content:
 *         application/json:
 *           example:
 *             request_type: Type4
 *             request_date: 2023-10-03
 *             request_time: 15:00:00
 *             student_id: 1
 *             department_id: 1
 *             status: Pending
 *             description: Request description goes here
 *     responses:
 *       201:
 *         description: Request created
 *       500:
 *         description: Error creating request
 */
router.post("/requests", createRequest);

/**
 * @swagger
 * /requests/close/{id}:
 *   put:
 *     summary: Edit a request
 *     requestBody:
 *       description: User object
 *       required: true
 *       content:
 *         application/json:
 *           example:
 *             status: Closed
 *     parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        schema:
 *          type: integer
 *     responses:
 *       200:
 *         description: Request edited
 *       500:
 *         description: Error editing request
 */
router.put("/requests/close/:id", editRequest);

module.exports = router;
